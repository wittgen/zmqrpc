#!/bin/sh

rm -rf build.arm32 build.x86
source /opt/rh/devtoolset-7/enable
source /opt/rce/setup.sh
export CENTOS7_ARM32_ROOT=/opt/rce/rootfs/centos7

mkdir build.x86
mkdir build.arm32

(cd build.x86 && cmake3 .. && make -j8)
(cd build.arm32 && cmake3 .. -DCMAKE_TOOLCHAIN_FILE=../toolchain/rce-arm32 && make -j8)

