#include <ZmqRpc.hpp>
#include <variant.hpp>
void callback(const variant32 &params,variant32 &result) {
  result["message"]="World";
  result["params"]=params;
}


int main (int argc,char *argv[]) {

  RpcServer server;
  server.addCall("mytest",callback);
  server.bind("tcp://*:5555");
  server.run();
  return 0;

}
