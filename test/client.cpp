#include <ZmqRpc.hpp>
#include <variant.hpp>

#include <stdlib.h>
#include <unistd.h>

int main (int argc,char *argv[]) {
  std::string conn("tcp://localhost:5555");
  int iter=1;
  int size=1;
  bool dump=false;
  char c;
  while ((c = getopt (argc, argv, "c:n:s:dh")) != -1) {
    switch(c) {
    case 'c': conn=std::string(optarg);
      break;
    case 'n': iter=atoi(optarg);
      break;
    case 's': size=atoi(optarg);
      break;
    case 'd': dump=true;
      break;
    case 'h':
    default:
      std::cout << "usage: client -c tcp://ip:port -n NITER -s SIZE -h -d \n";
      return -1;
    }
  }
  
  RpcClient client;
  std::cout << "Connecting to " << conn << std::endl;
  client.connect(conn);
  for(int i=0;i<iter;i++) {
    variant32 params,result;
    params["message"]="Hello";  
    params["data"]=std::vector<uint32_t>(size);
    std::cout << "bytes sent: " << params.byte_count() << std::endl;
    client.call("mytest",params,result);
    std::cout << "bytes recv: " << result.byte_count() << std::endl;    
    if(dump) result.dump();
  }
}
