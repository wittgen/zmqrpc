#include <string>
#include <zmq.hpp>
#include <iostream>
#include <unistd.h>
#include <variant.hpp>
#include <MsgPack.hpp>
#include <functional>

class RpcServer  {
public:
  using func_type=std::function<void(const variant32 &,variant32 &)>;
  RpcServer() :socket(context,ZMQ_REP) {}
  void bind(const std::string &conn) {
    socket.bind(conn);
  } 
  
  void run() {
     while (true) {
        zmq::message_t request;
        socket.recv (&request);
	MemoryReadBuffer buffer((char*)request.data(),request.size());
	variant32 param;
	variant32 result;
	msgpack::Reader32(buffer,param);
	call(param,result);	
	std::vector<uint8_t> wbuffer;
	MemoryWriteBuffer wb(wbuffer);
	try {
	  msgpack::Writer32(wb,result);
	} 
	catch (const char *e) {
	  std::cerr << e << std::endl;
	} 
        zmq::message_t reply (wbuffer.data(),wbuffer.size());
        socket.send (reply);
     }
  }
  inline static void addCall(const std::string &name,func_type f) {
    call_map[name]=f;
  }
private:
  zmq::socket_t socket;
  static inline zmq::context_t context=zmq::context_t(1);
  static inline std::map<std::string,func_type> call_map;
  void call(const variant32 &rpc,variant32 &result) {
    const std::string &method=rpc["method"];
    const variant32 &params=rpc["params"];
    uint32_t id=rpc["id"];
    func_type f=call_map.at(method);
    f(params,result);
  }
};

class RpcClient {
public:
  RpcClient() : socket(context,ZMQ_REQ)  {};
  void connect(const std::string conn) {
    socket.connect(conn);
  }  
  void call(const std::string method,const variant32 &params,variant32 &result) {
    variant32 v;
    v["method"]=method;
    v["params"]=params;
    v["id"]=uint32_t(1);
    std::vector<uint8_t> wbuffer;
    MemoryWriteBuffer wb(wbuffer);
    msgpack::Writer32(wb,v);
    zmq::message_t request (wbuffer.data(),wbuffer.size());
    socket.send (request);
    zmq::message_t reply;
    socket.recv (&reply);
    MemoryReadBuffer buffer((char*)reply.data(),reply.size());
    try {
      msgpack::Reader32(buffer,result);    
    } catch (const char *e) {
      std::cerr << e << std::endl;
    }
  }
private:
   zmq::socket_t socket;
  static inline zmq::context_t context=zmq::context_t(1);
};
